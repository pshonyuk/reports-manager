const gulp = require('gulp'),
	sass = require('gulp-sass'),
	ts = require('gulp-typescript'),
	useref = require('gulp-useref'),
	uglify = require('gulp-uglify'),
	gulpIf = require('gulp-if'),
	cssnano = require('gulp-cssnano'),
	imagemin = require('gulp-imagemin'),
	cache = require('gulp-cache'),
	del = require('del'),
	runSequence = require('run-sequence'),
	sourcemaps = require('gulp-sourcemaps');

const paths = {
	'assets': './app/src/assets/',
	'scripts': './app/src/scripts/',
	'styles': './app/src/styles/',
	'build': './app/build/',
	'tsconfig': './tsconfig.json'
};


const tsProject = ts.createProject('./tsconfig.json');

gulp.task('sass', function () {
	return gulp.src(`${paths.styles}/**/*.scss`)
		.pipe(sass())
		.pipe(gulp.dest(`${paths.build}styles/`));
});


gulp.task('useref', function () {
	return gulp.src('app/*.html')
		.pipe(useref())
		.pipe(gulpIf('*.js', uglify()))
		.pipe(gulpIf('*.css', cssnano()))
		.pipe(gulp.dest(paths.build));
});

gulp.task('images', function () {
	return gulp.src(`${paths.assets}images/**/*.+(png|jpg|gif|svg)`)
		.pipe(cache(imagemin()))
		.pipe(gulp.dest(`${paths.build}/assets/images`));
});

gulp.task('fonts', function () {
	return gulp.src(`${paths.assets}fonts/**/*`)
		.pipe(gulp.dest(`${paths.build}assets/fonts`));
});

gulp.task('clean:dist', function () {
	return del.sync(paths.build);
});

gulp.task('cache:clear', function (callback) {
	return cache.clearAll(callback);
});

gulp.task('typescript', function () {
	const tsResult = tsProject.src()
		.pipe(sourcemaps.init())
		.pipe(tsProject());

	return tsResult.js
		.pipe(sourcemaps.write({
			sourceRoot: paths.scripts,
			includeContent: false
		}))
		.pipe(gulp.dest(`${paths.build}scripts/`));
});


gulp.task('watch', ['sass', 'typescript'], function () {
	gulp.watch(`${paths.styles}**/*.scss`, ['sass']);
	gulp.watch(`${paths.scripts}**/*.ts`, ['typescript']);
});

gulp.task('build', function (callback) {
	runSequence(
		'clean:dist',
		'typescript',
		'sass',
		['useref', 'images', 'fonts'],
		callback);
});

gulp.task('default', ['build', 'watch']);