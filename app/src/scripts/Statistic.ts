import * as moment from 'moment';
import * as path from 'path';
import * as fs from 'fs';
import * as $ from 'jquery';
import { ReportsFilesManager, IReportItemData } from './ReportsFilesManager';
import { RxEventEmitter } from './utils';

export class Statistic {
  public static DATE_PICKER_SELECTOR = '#statistic-date-picker';
  public static TYPE_SELECT_SELECTOR = '#statistic-chart-type';
  public controllers: any[];
  public reportsSubject: RxEventEmitter;
  public typeSubject: RxEventEmitter;
  public dateSubject: RxEventEmitter;
  public reportsFilesManager: ReportsFilesManager;

  constructor() {
    this.controllers = [];
    this.reportsFilesManager = new ReportsFilesManager();
    this.dateSubject = new RxEventEmitter();
    this.typeSubject = new RxEventEmitter();
    this.reportsSubject = new RxEventEmitter();

    this.reportsFilesManager.listSubject.subscribe(this.updateReportsSubject);
    this.dateSubject.subscribe(this.updateReportsSubject);

    this.initDatePicker();
    this.initChartTypeSelector();
    this.connectControllers();
  }

  private initDatePicker(): void {
    $.fn.datepicker.language['uk'] = {
      days: ['Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятница', 'Субота'],
      daysShort: ['Нед', 'Пон', 'Вів', 'Сер', 'Чет', 'П\'ят', 'Суб'],
      daysMin: ['Нд', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
      months: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
      monthsShort: ['Січ', 'Лют', 'Бер', 'Кві', 'Тра', 'Чер', 'Лип', 'Сер', 'Вер', 'Жов', 'Лис', 'Гру'],
      today: 'Сьогодні',
      clear: 'Очистити',
      dateFormat: 'dd.mm.yyyy',
      timeFormat: 'hh:ii',
      firstDay: 1
    };

    (<any>$(Statistic.DATE_PICKER_SELECTOR))
      .datepicker({
        range: true,
        language: 'uk',
        maxDate: moment().endOf('day').toDate(),
        onSelect: (formattedDates, simpleDates) => {
          simpleDates || (simpleDates = []);
          simpleDates[0] || (simpleDates[0] = simpleDates[1] || Date.now());
          simpleDates[1] || (simpleDates[1] = simpleDates[0] || Date.now());
          const dateRange = simpleDates.map((date) => moment(date).endOf('day').toDate());
          this.dateSubject.onNext(dateRange);
        }
      })
      .data('datepicker')
      .selectDate([
        moment().startOf('month').toDate(),
        moment().endOf('day').toDate()
      ]);
  }

  private updateReportsSubject = () => {
    this.reportsSubject.onNext(this.getReportsByDateRange(this.dateSubject.getValue()));
  };

  private initChartTypeSelector(): void {
    $(Statistic.TYPE_SELECT_SELECTOR).on('change', (e: Event) => {
      this.typeSubject.onNext($(e.target).val());
    })
  }

  private connectControllers(): void {
    const controllersPaths: string[] = fs.readdirSync(path.join(__dirname, 'statistic')) || [],
      ControllersModules: NodeModule[] = controllersPaths.map((controllerPath) => {
        return require(path.join(__dirname, 'statistic', controllerPath));
      });

    ControllersModules.forEach((ControllersModule) => {
      (<any>Object).values(ControllersModule)
        .filter((exportItem) => typeof exportItem === 'function')
        .forEach((Controller) => {
          this.controllers.push(new Controller(this.reportsSubject, this.typeSubject));
        });
    });
  }

  private getReportsByDateRange(datesRange): IReportItemData[] {
    if(!datesRange) return [];
    const timestampRange = datesRange.map(date => +(new Date(date))).sort();

    return this.reportsFilesManager.listSubject.getValue().filter((report: IReportItemData) => {
      const createdAt = +report.createdAt;
      return createdAt >= timestampRange[0] && createdAt <= timestampRange[1];
    });
  }
}
