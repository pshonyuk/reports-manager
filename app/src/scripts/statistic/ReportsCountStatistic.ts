import { Chart, ChartData } from 'chart.js';
import { groupBy } from 'lodash';
import { generateColorHash, hexToRgb, RxEventEmitter } from '../utils';
import { IReportItemData } from '../ReportsFilesManager';

export class AuthorStatistic {
  public static CONTAINER: HTMLElement = <HTMLElement>document.querySelector('#author-statistic');
  public static CANVAS_CONTAINER: HTMLCanvasElement = <HTMLCanvasElement>AuthorStatistic.CONTAINER.querySelector('canvas');
  public reportsSubject: RxEventEmitter;
  public typeSubject: RxEventEmitter;
  public chart: Chart;

  constructor(dateSubject: RxEventEmitter, typeSubject: RxEventEmitter) {
    this.reportsSubject = dateSubject;
    this.typeSubject = typeSubject;

    this.renderChart();
    this.subscribeUpdate();
  }

  private subscribeUpdate() {
    this.reportsSubject.subscribe(this.renderChart);
    this.typeSubject.subscribe(this.renderChart);
  }

  private renderChart = () => {
    if(this.chart) this.chart.destroy();
    this.chart = new Chart(AuthorStatistic.CANVAS_CONTAINER, {
      type: this.typeSubject.getValue() || 'bar',
      data: this.getChartData(),
      options: <any>{
        maintainAspectRatio: false,
        responsive: true,
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 50,
            bottom: 0
          }
        }
      }
    });
  };

  private getChartData(): ChartData {
    const reports = this.reportsSubject.getValue(),
      reportsGroups: any = groupBy(reports, (report: IReportItemData) => report.author.toLowerCase().trim()),
      reportsGroupsValues: IReportItemData[][] = (<any>Object).values(reportsGroups),
      labels = [
        'Всього',
        ...reportsGroupsValues.map((reports: IReportItemData[]) => reports[0].author)
      ],
      data = [
        reports.length,
        ...reportsGroupsValues.map((reports: IReportItemData[]) => reports.length)
      ],
      backgroundColor = labels.map(generateColorHash).map((hex) => {
        const rgb = hexToRgb(hex);
        return `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0.2)`;
      });

    return {
      labels,
      datasets: [{
        label: 'Кількість поданих звітів',
        backgroundColor,
        data
      }]
    };
  }
}
