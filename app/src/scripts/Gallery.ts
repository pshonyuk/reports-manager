import * as $ from 'jquery';
import * as fs from 'fs';
import * as path from 'path';

export class Gallery {
  private static PATH = path.join(__dirname, '../../media');
  public container: HTMLElement;

  constructor(selector: string) {
    this.container = <HTMLElement>document.querySelector(selector);
    this.render();
  }

  render(): void {
    const galleryTpl: string = this.getImagesFiles()
      .map((fileName: string) => {
        return `<img src="${fileName}"/>`
      })
      .join('');

    this.container.innerHTML = galleryTpl;
    this.initFotorama();
  }

  getImagesFiles(): string[] {
    const accessExtensions = ['.jpg', '.jpeg', '.png', '.bmp'];

    return (fs.readdirSync(Gallery.PATH) || [])
      .filter(fileName => accessExtensions.indexOf(path.extname(fileName).toLowerCase()) >= 0)
      .sort((a: string, b: string)=> {
        const aName = path.basename(a).toLowerCase().trim(),
          bName = path.basename(b).toLowerCase().trim();

        if(aName < bName) return -1;
        if(aName > bName) return 1;
        return 0;
      })
      .map(fileName => path.join(__dirname, `../../media/${fileName}`));
  }

  initFotorama() {
    (<any>$(this.container)).fotorama({
      width: '100%',
      maxwidth: '100%',
      fit: 'cover',
      keyboard: true,
      transition: 'dissolve',
      maxheight: window.innerHeight - 110,
      allowfullscreen: true,
      nav: 'thumbs'
    })
  }
}
