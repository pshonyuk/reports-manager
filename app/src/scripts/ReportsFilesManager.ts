const fs = require('fs');
const path = require('path');
const homedir = require('homedir');
import { RxEventEmitter } from './utils';


export interface IReportItemData {
  fd: string;
  name: string;
  body: string;
  createdAt: number;
  author: string;
}


export class ReportsFilesManager {
  public static storeDir: string = path.join(homedir(), 'reports');
  public static fileExt: string = '.dat';
  public static uid: number = 0;
  public static getUniqueFd(): string {
    return `${Date.now()}_${ReportsFilesManager.uid++}${ReportsFilesManager.fileExt}`;
  }
  private static instance: ReportsFilesManager;

  public storeDir: string;
  public listSubject: RxEventEmitter;

  constructor() {
    if(ReportsFilesManager.instance) return ReportsFilesManager.instance;

    ReportsFilesManager.instance = this;
    this.storeDir = path.resolve(ReportsFilesManager.storeDir);
    this.listSubject = new RxEventEmitter();
    this.updateList();
  }

  public updateList(): void {
    this.prepareStoreDir();

    const filesList: IReportItemData[] = (fs.readdirSync(this.storeDir) || [])
      .filter((fileName: string) => path.extname(fileName) === ReportsFilesManager.fileExt)
      .map((fileName: string) => ({
        fd: fileName,
        file: fs.readFileSync(path.join(this.storeDir, fileName), 'utf8')
      }))
      .map((fileData: {fd: string, file: string}) => {
        try {
          const jsonData = JSON.parse(fileData.file);
          return <IReportItemData>{
            fd: fileData.fd,
            ...jsonData,
            author: jsonData.author || 'Невизначено'
          }
        } catch (e) {
          return null;
        }
      });

    this.listSubject.onNext(filesList);
  }

  public add(reportData: IReportItemData): void {
    const writeData = {...reportData};

    delete writeData.fd;

    this.prepareStoreDir();
    fs.writeFileSync(path.join(this.storeDir, reportData.fd), JSON.stringify(writeData));
    this.updateList();
  }

  public update(reportData: IReportItemData): void {
    this.add(reportData);
  }

  public remove(reportData: IReportItemData): void {
    this.prepareStoreDir();
    fs.unlinkSync(path.join(this.storeDir, reportData.fd));
    this.updateList();
  }

  private prepareStoreDir(): void {
    if (!fs.existsSync(this.storeDir)) {
      fs.mkdirSync(this.storeDir);
    }
  }
}
