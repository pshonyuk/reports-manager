"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const $ = require("jquery");
const moment = require("moment");
const ReportsFilesManager_1 = require("../ReportsFilesManager");
function displayCreatedDate(timestamp) {
    moment.locale('uk');
    return moment(timestamp).format('ll');
}
exports.displayCreatedDate = displayCreatedDate;
class ReportsValuesStatistic {
    constructor() {
        this.onToggleTab = () => {
            const $typeSelector = $('#statistic-chart-type'), $dateSelectorWrapper = $('.datepicker-wrap');
            this.show = ReportsValuesStatistic.CONTAINER.classList.contains('active');
            if (this.show) {
                $typeSelector.selectpicker('hide');
                $dateSelectorWrapper.hide();
            }
            else {
                $typeSelector.selectpicker('show');
                $dateSelectorWrapper.show();
            }
            this.renderReportsSelect();
        };
        this.renderReportsSelect = () => {
            let selected = null;
            if (this.$reportsSelect) {
                selected = this.$reportsSelect.val();
                this.$reportsSelect.remove();
                this.$reportsSelect = null;
            }
            if (!this.show)
                return;
            this.$reportsSelect = $(this.generateReportsSelectTpl(selected));
            $('#statistic .manage-bar').prepend(this.$reportsSelect);
            this.$reportsSelect
                .find('select')
                .selectpicker({
                multiple: false,
                noneSelectedText: 'Виберіть звіт'
            })
                .on('change', this.renderReport);
        };
        this.renderReport = () => {
            this.tinyMCE.setContent(this.getComputedContent());
        };
        this.reportsFilesManager = new ReportsFilesManager_1.ReportsFilesManager();
        this.show = false;
        this
            .initTinyMCE()
            .then((editors) => {
            this.tinyMCE = editors[0];
            $(ReportsValuesStatistic.TABS).on('shown.bs.tab', this.onToggleTab);
            $(ReportsValuesStatistic.TABS).on('hidden.bs.tab', this.onToggleTab);
            this.renderReport();
            this.subscribeUpdate();
        })
            .catch((err) => {
            console.error(err);
        });
    }
    initTinyMCE() {
        let fontSizeFormats = '';
        for (let i = 8; i <= 48; i += 2) {
            fontSizeFormats += `${i}pt `;
        }
        return window.tinymce.init({
            selector: ReportsValuesStatistic.TEXT_AREA_SELECTOR,
            height: 500,
            theme: 'modern',
            language: 'uk_UA',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking table contextmenu directionality',
                'emoticons paste textcolor colorpicker textpattern imagetools codesample toc help'
            ],
            fontsize_formats: fontSizeFormats.trim(),
            toolbar1: 'undo redo | sizeselect | bold italic | fontselect | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help'
        });
    }
    generateReportsSelectTpl(selected) {
        return [
            '<div class="col-md-3">',
            '<select>',
            this
                .reportsFilesManager
                .listSubject
                .getValue()
                .sort((a, b) => {
                if (!a.createdAt)
                    return -1;
                if (!b.createdAt)
                    return 1;
                if (a.createdAt > b.createdAt)
                    return -1;
                if (a.createdAt < b.createdAt)
                    return 1;
                return 0;
            })
                .map((report) => `
          <option ${selected === report.fd ? 'selected' : ''} value="${report.fd}">
            ${report.name}
            ${!report.author ? '' : `
              <span class="badge badge-default">
                &nbsp-&nbsp${report.author}
              </span>
            `}
            ${!report.createdAt ? '' : `
              <span class="badge badge-default">
                &nbsp(${displayCreatedDate(report.createdAt)})
              </span>
            `}
          </option>`),
            '</select>',
            '</div>'
        ].join('');
    }
    subscribeUpdate() {
        this.reportsFilesManager.listSubject.subscribe(this.renderReportsSelect);
        this.reportsFilesManager.listSubject.subscribe(this.renderReport);
    }
    getComputedContent() {
        const activeReport = this.getActiveReport(), activeContent = activeReport ? activeReport.body : '', style = 'text-align: center;font-weight: 700;background-color: #257cb6;color: #fff;', $activeContent = $(`<div>${activeContent}</div>`);
        $activeContent.find('tr').each((i, tr) => {
            const $tr = $(tr);
            let totals = {};
            $tr.find('td').each((i, td) => {
                const textContent = (td.textContent || '0').trim() || '0', val = parseFloat(textContent.replace(',', '.').replace(/\s+/g, '')), prefixMatch = textContent.match(/[^\d\s.,]+$/), prefix = prefixMatch ? prefixMatch[0] : '';
                if (isNaN(val)) {
                    totals[prefix] = void 0;
                    return;
                }
                isNaN(totals[prefix]) && (totals[prefix] = 0);
                totals[prefix] += val;
            });
            let $appendedTd = null;
            Object.keys(totals).forEach((prefix) => {
                const total = totals[prefix];
                if (!isNaN(total) && total !== null) {
                    if (!$appendedTd) {
                        $appendedTd = $(`<td style="${style}"></td>`);
                        $tr.append($appendedTd);
                    }
                    $appendedTd.append(`<p>${prefix ? prefix + ' - ' : ''}${total}</p>`);
                }
            });
        });
        return $activeContent.html();
    }
    getActiveReport() {
        const selected = this.$reportsSelect && this.$reportsSelect.find('select').val();
        return this
            .reportsFilesManager
            .listSubject
            .getValue()
            .filter((report) => report.fd === selected)[0] || null;
    }
}
ReportsValuesStatistic.CONTAINER = document.querySelector('#reports-values-statistic');
ReportsValuesStatistic.TABS = document.querySelector('.statistic-list > .nav');
ReportsValuesStatistic.TEXT_AREA_SELECTOR = '#reports-values-statistic textarea';
exports.ReportsValuesStatistic = ReportsValuesStatistic;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zY3JpcHRzL3N0YXRpc3RpYy9SZXBvcnRzVmFsdWVzU3RhdGlzdGljLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsNEJBQTRCO0FBQzVCLGlDQUFpQztBQUVqQyxnRUFBOEU7QUFFOUUsNEJBQW1DLFNBQWlCO0lBQ2xELE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDeEMsQ0FBQztBQUhELGdEQUdDO0FBRUQ7SUFTRTtRQTRDUSxnQkFBVyxHQUFHO1lBQ3BCLE1BQU0sYUFBYSxHQUFHLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxFQUM5QyxvQkFBb0IsR0FBRyxDQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUMvQyxJQUFJLENBQUMsSUFBSSxHQUFHLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRTFFLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNiLGFBQWEsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ25DLG9CQUFvQixDQUFDLElBQUksRUFBRSxDQUFDO1lBQzlCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixhQUFhLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNuQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM5QixDQUFDO1lBRUQsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDO1FBRU0sd0JBQW1CLEdBQUc7WUFDNUIsSUFBSSxRQUFRLEdBQVcsSUFBSSxDQUFDO1lBRTVCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDckMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDN0IsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFBQyxNQUFNLENBQUM7WUFFdkIsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDakUsQ0FBQyxDQUFDLHdCQUF3QixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN6RCxJQUFJLENBQUMsY0FBYztpQkFDaEIsSUFBSSxDQUFDLFFBQVEsQ0FBQztpQkFDZCxZQUFZLENBQUM7Z0JBQ1osUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsZ0JBQWdCLEVBQUUsZUFBZTthQUNsQyxDQUFDO2lCQUNELEVBQUUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3JDLENBQUMsQ0FBQztRQTBDTSxpQkFBWSxHQUFHO1lBQ3JCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLENBQUM7UUFDckQsQ0FBQyxDQUFDO1FBMUhBLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLHlDQUFtQixFQUFFLENBQUM7UUFDckQsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7UUFFbEIsSUFBSTthQUNELFdBQVcsRUFBRTthQUNiLElBQUksQ0FBQyxDQUFDLE9BQU87WUFDWixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUUxQixDQUFDLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDcEUsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBRXJFLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUNwQixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDekIsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLENBQUMsR0FBVTtZQUNoQixPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLFdBQVc7UUFDakIsSUFBSSxlQUFlLEdBQUcsRUFBRSxDQUFDO1FBRXpCLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztZQUMvQixlQUFlLElBQUksR0FBRyxDQUFDLEtBQUssQ0FBQztRQUMvQixDQUFDO1FBRUQsTUFBTSxDQUFPLE1BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ2hDLFFBQVEsRUFBRSxzQkFBc0IsQ0FBQyxrQkFBa0I7WUFDbkQsTUFBTSxFQUFFLEdBQUc7WUFDWCxLQUFLLEVBQUUsUUFBUTtZQUNmLFFBQVEsRUFBRSxPQUFPO1lBQ2pCLE9BQU8sRUFBRTtnQkFDUCw2RUFBNkU7Z0JBQzdFLGtFQUFrRTtnQkFDbEUsbUVBQW1FO2dCQUNuRSxrRkFBa0Y7YUFDbkY7WUFDRCxnQkFBZ0IsRUFBRSxlQUFlLENBQUMsSUFBSSxFQUFFO1lBQ3hDLFFBQVEsRUFBRSxrS0FBa0s7WUFDNUssUUFBUSxFQUFFLHVFQUF1RTtTQUNsRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBdUNPLHdCQUF3QixDQUFDLFFBQWdCO1FBQy9DLE1BQU0sQ0FBQztZQUNMLHdCQUF3QjtZQUN4QixVQUFVO1lBQ1YsSUFBSTtpQkFDRCxtQkFBbUI7aUJBQ25CLFdBQVc7aUJBQ1gsUUFBUSxFQUFFO2lCQUNWLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNULEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztvQkFBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztvQkFBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUM7b0JBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN6QyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUM7b0JBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNYLENBQUMsQ0FBQztpQkFDRCxHQUFHLENBQUMsQ0FBQyxNQUF1QixLQUFLO29CQUN0QixRQUFRLEtBQUssTUFBTSxDQUFDLEVBQUUsR0FBRyxVQUFVLEdBQUcsRUFBRSxXQUFXLE1BQU0sQ0FBQyxFQUFFO2NBQ2xFLE1BQU0sQ0FBQyxJQUFJO2NBQ1gsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsR0FBRzs7NkJBRVAsTUFBTSxDQUFDLE1BQU07O2FBRTdCO2NBQ0MsQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLEVBQUUsR0FBRzs7d0JBRWYsa0JBQWtCLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQzs7YUFFL0M7b0JBQ08sQ0FDWDtZQUNILFdBQVc7WUFDWCxRQUFRO1NBQ1QsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDYixDQUFDO0lBRU8sZUFBZTtRQUNyQixJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDcEUsQ0FBQztJQU1PLGtCQUFrQjtRQUN4QixNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFLEVBQ3pDLGFBQWEsR0FBRyxZQUFZLEdBQUcsWUFBWSxDQUFDLElBQUksR0FBRyxFQUFFLEVBQ3JELEtBQUssR0FBQyw0RUFBNEUsRUFDbEYsY0FBYyxHQUFHLENBQUMsQ0FBQyxRQUFRLGFBQWEsUUFBUSxDQUFDLENBQUM7UUFFcEQsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUNuQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDbEIsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO1lBRWhCLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3hCLE1BQU0sV0FBVyxHQUFHLENBQUMsRUFBRSxDQUFDLFdBQVcsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxHQUFHLEVBQ3ZELEdBQUcsR0FBRyxVQUFVLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUNuRSxXQUFXLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsRUFDOUMsTUFBTSxHQUFHLFdBQVcsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUU3QyxFQUFFLENBQUEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNkLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQztvQkFDeEIsTUFBTSxDQUFDO2dCQUNULENBQUM7Z0JBQ0QsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM5QyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQ2pCLE1BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTTtnQkFDeEMsTUFBTSxLQUFLLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM3QixFQUFFLENBQUEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDbkMsRUFBRSxDQUFBLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO3dCQUNoQixXQUFXLEdBQUcsQ0FBQyxDQUFDLGNBQWMsS0FBSyxTQUFTLENBQUMsQ0FBQzt3QkFDOUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDMUIsQ0FBQztvQkFDRCxXQUFXLENBQUMsTUFBTSxDQUFDLE1BQU0sTUFBTSxHQUFHLE1BQU0sR0FBRyxLQUFLLEdBQUcsRUFBRSxHQUFHLEtBQUssTUFBTSxDQUFDLENBQUM7Z0JBQ3ZFLENBQUM7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRU8sZUFBZTtRQUNyQixNQUFNLFFBQVEsR0FBVyxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ3pGLE1BQU0sQ0FBQyxJQUFJO2FBQ1IsbUJBQW1CO2FBQ25CLFdBQVc7YUFDWCxRQUFRLEVBQUU7YUFDVixNQUFNLENBQUMsQ0FBQyxNQUF1QixLQUFLLE1BQU0sQ0FBQyxFQUFFLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDO0lBQzVFLENBQUM7O0FBcExhLGdDQUFTLEdBQTZCLFFBQVEsQ0FBQyxhQUFhLENBQUMsMkJBQTJCLENBQUMsQ0FBQztBQUMxRiwyQkFBSSxHQUF5QyxRQUFRLENBQUMsYUFBYSxDQUFDLHdCQUF3QixDQUFDLENBQUM7QUFDOUYseUNBQWtCLEdBQVcsb0NBQW9DLENBQUM7QUFIbEYsd0RBc0xDIiwiZmlsZSI6InN0YXRpc3RpYy9SZXBvcnRzVmFsdWVzU3RhdGlzdGljLmpzIiwic291cmNlUm9vdCI6Ii4vYXBwL3NyYy9zY3JpcHRzLyJ9
