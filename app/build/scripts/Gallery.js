"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const $ = require("jquery");
const fs = require("fs");
const path = require("path");
class Gallery {
    constructor(selector) {
        this.container = document.querySelector(selector);
        this.render();
    }
    render() {
        const galleryTpl = this.getImagesFiles()
            .map((fileName) => {
            return `<img src="${fileName}"/>`;
        })
            .join('');
        this.container.innerHTML = galleryTpl;
        this.initFotorama();
    }
    getImagesFiles() {
        const accessExtensions = ['.jpg', '.jpeg', '.png', '.bmp'];
        return (fs.readdirSync(Gallery.PATH) || [])
            .filter(fileName => accessExtensions.indexOf(path.extname(fileName).toLowerCase()) >= 0)
            .sort((a, b) => {
            const aName = path.basename(a).toLowerCase().trim(), bName = path.basename(b).toLowerCase().trim();
            if (aName < bName)
                return -1;
            if (aName > bName)
                return 1;
            return 0;
        })
            .map(fileName => path.join(__dirname, `../../media/${fileName}`));
    }
    initFotorama() {
        $(this.container).fotorama({
            width: '100%',
            maxwidth: '100%',
            fit: 'cover',
            keyboard: true,
            transition: 'dissolve',
            maxheight: window.innerHeight - 110,
            allowfullscreen: true,
            nav: 'thumbs'
        });
    }
}
Gallery.PATH = path.join(__dirname, '../../media');
exports.Gallery = Gallery;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zY3JpcHRzL0dhbGxlcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw0QkFBNEI7QUFDNUIseUJBQXlCO0FBQ3pCLDZCQUE2QjtBQUU3QjtJQUlFLFlBQVksUUFBZ0I7UUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBZ0IsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUVELE1BQU07UUFDSixNQUFNLFVBQVUsR0FBVyxJQUFJLENBQUMsY0FBYyxFQUFFO2FBQzdDLEdBQUcsQ0FBQyxDQUFDLFFBQWdCO1lBQ3BCLE1BQU0sQ0FBQyxhQUFhLFFBQVEsS0FBSyxDQUFBO1FBQ25DLENBQUMsQ0FBQzthQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUVaLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQztRQUN0QyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELGNBQWM7UUFDWixNQUFNLGdCQUFnQixHQUFHLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFFM0QsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3hDLE1BQU0sQ0FBQyxRQUFRLElBQUksZ0JBQWdCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdkYsSUFBSSxDQUFDLENBQUMsQ0FBUyxFQUFFLENBQVM7WUFDekIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFDakQsS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUM7WUFFaEQsRUFBRSxDQUFBLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztnQkFBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsRUFBRSxDQUFBLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztnQkFBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzNCLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7YUFDRCxHQUFHLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLGVBQWUsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFFRCxZQUFZO1FBQ0osQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUUsQ0FBQyxRQUFRLENBQUM7WUFDaEMsS0FBSyxFQUFFLE1BQU07WUFDYixRQUFRLEVBQUUsTUFBTTtZQUNoQixHQUFHLEVBQUUsT0FBTztZQUNaLFFBQVEsRUFBRSxJQUFJO1lBQ2QsVUFBVSxFQUFFLFVBQVU7WUFDdEIsU0FBUyxFQUFFLE1BQU0sQ0FBQyxXQUFXLEdBQUcsR0FBRztZQUNuQyxlQUFlLEVBQUUsSUFBSTtZQUNyQixHQUFHLEVBQUUsUUFBUTtTQUNkLENBQUMsQ0FBQTtJQUNKLENBQUM7O0FBOUNjLFlBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxhQUFhLENBQUMsQ0FBQztBQUQ1RCwwQkFnREMiLCJmaWxlIjoiR2FsbGVyeS5qcyIsInNvdXJjZVJvb3QiOiIuL2FwcC9zcmMvc2NyaXB0cy8ifQ==
