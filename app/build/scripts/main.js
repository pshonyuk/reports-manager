"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ReportsList_1 = require("./ReportsList");
const ReportCreator_1 = require("./ReportCreator");
const ReportEditor_1 = require("./ReportEditor");
const Statistic_1 = require("./Statistic");
const Gallery_1 = require("./Gallery");
class Application {
    constructor() {
        this.reportEditor = new ReportEditor_1.ReportEditor();
        this.reportsList = new ReportsList_1.ReportsList();
        this.reportCreator = new ReportCreator_1.ReportCreator();
        this.reportCreator.getBasicTemplate();
        this.gallery = new Gallery_1.Gallery('#multimedia');
        this.statistic = new Statistic_1.Statistic();
    }
}
exports.app = new Application();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zY3JpcHRzL21haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwrQ0FBNEM7QUFDNUMsbURBQWdEO0FBQ2hELGlEQUE4QztBQUM5QywyQ0FBd0M7QUFDeEMsdUNBQW9DO0FBRXBDO0lBT0U7UUFDRSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksMkJBQVksRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSx5QkFBVyxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztRQUN6QyxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDdEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLGlCQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLHFCQUFTLEVBQUUsQ0FBQztJQUNuQyxDQUFDO0NBQ0Y7QUFHWSxRQUFBLEdBQUcsR0FBZ0IsSUFBSSxXQUFXLEVBQUUsQ0FBQyIsImZpbGUiOiJtYWluLmpzIiwic291cmNlUm9vdCI6Ii4vYXBwL3NyYy9zY3JpcHRzLyJ9
