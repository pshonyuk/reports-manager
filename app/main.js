const electron = require("electron"),
	path = require("path"),
	iconPath = path.resolve(path.join(__dirname, "app-icons/icon.png")),
	{ BrowserWindow, Menu, app } = electron;

let mainWindow = null;


app.on("window-all-closed", function () {
	if (process.platform != "darwin") app.quit();
});

app.on("ready", function () {
	mainWindow = new BrowserWindow({
		icon: iconPath,
		minWidth: 620,
		minHeight: 500,
		show: false
	});

	mainWindow.webContents.session.clearCache(() => {});
	mainWindow.loadURL("file://" + __dirname + "/index.html");
	mainWindow.maximize();

	mainWindow.webContents.on('did-finish-load', function () {
		mainWindow.show();
	});

	mainWindow.on("closed", function () {
		mainWindow = null;
	});
});
